#!/usr/bin/env bash

PACKAGE="pcre"

. ${0%/*}/../common/common.inc.sh

download "pcre" "ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.34.tar.bz2"

if [ $ISMINGW -eq 1 ]; then
    CONFIGURE_FLAGS+=" --enable-shared --disable-static"
else
    CONFIGURE_FLAGS+=" --disable-shared --enable-static --with-pic"
fi

extract_archives

pushd pcre*
echo_action "building pcre"
./configure --prefix=$TARGET_DIR $CONFIGURE_FLAGS \
    --enable-utf \
    --disable-cpp \
    --enable-newline-is-anycrlf
$MAKE -j $JOBS install
popd

finish_libs
