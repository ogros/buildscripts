#!/usr/bin/env bash

PACKAGE="curl"

. ${0%/*}/../common/common.inc.sh

download "curl" "http://curl.haxx.se/download/curl-7.35.0.tar.bz2"

if [ $ISMINGW -eq 1 ]; then
    CONFIGURE_FLAGS+=" --enable-shared --disable-static"
else
    CONFIGURE_FLAGS+=" --disable-shared --enable-static --with-pic"
fi

extract_archives

pushd curl*
echo_action "building curl"
./configure --prefix=$TARGET_DIR $CONFIGURE_FLAGS \
    --enable-threaded-resolver \
    --disable-manual \
    --disable-ldap \
    --disable-ldaps \
    --disable-rtsp \
    --without-ssl \
    --without-libssh2 \
    --without-libidn \
    --without-librtmp
$MAKE -j $JOBS install
popd

finish_libs
