#!/usr/bin/env bash

PACKAGE="portaudio"

. ${0%/*}/../common/common.inc.sh

download "portaudio" "http://www.portaudio.com/archives/pa_stable_v19_20140130.tgz"

if [ $ISMINGW -eq 1 ]; then
    PA_CONFIGURE_FLAGS=" --enable-shared --disable-static --with-winapi=wmme,directx"
else
    PA_CONFIGURE_FLAGS=" --disable-shared --enable-static --with-pic"
    if [ $ISLINUX -eq 1 ]; then
        PA_CONFIGURE_FLAGS+=" --with-alsa --with-oss"
    elif [ $ISOSX -eq 1 ]; then
        PA_CONFIGURE_FLAGS+=" --disable-mac-universal"
    fi
fi

CONFIGURE_FLAGS+=$PA_CONFIGURE_FLAGS

extract_archives

pushd portaudio*
echo_action "building portaudio"
patch -p0 < $PATCH_DIR/configue_werror.patch
test $ISWCLANG -eq 1 && sed -i'' -e 's/-mthreads//g' configure
./configure --prefix=$TARGET_DIR $CONFIGURE_FLAGS --without-jack
# AR is already exported but portaudio still uses the wrong AR
test -n "$AR" && sed -i'' -e "s/\/usr\/bin\/ar/$AR/g" Makefile
test -n "$AR" && sed -i'' -e "s/\/usr\/bin\/ar/$AR/g" libtool
$MAKE -j $JOBS install
popd

finish_libs
