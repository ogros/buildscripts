# FFMPEG CROSS BUILD SCRIPT #

### SUPPORTED TARGETS ###

`linux32`, `linux64`, `mingw32`, `mingw64`, `w32-clang`, `w64-clang`, `osx32` and `osx64`

### SUPPORTED HOSTS ###

**Linux** and **OS X**

### REQUIREMENTS ###

A native C and C++ compiler (gcc or clang)  
Building for **Windows** on **Linux** requires mingw-w64 or [wclang](https://github.com/tpoechtrager/wclang)   
Building for **OS X** on **Linux** requires [OSXCross](https://github.com/tpoechtrager/osxcross)  

### BUILDING ###

`TARGET=<target> [HOST=<host>] [ENABLEGPL=1] ./build.sh`

Forcing a different compiler (native targets only):  

  * CXX=g++-4.4 CC=gcc-4.4 TARGET=linux64 ./build.sh

### CURRENT LIBRARIES ###

`zlib`, `bzip2`, `libogg`, `libvoris`, `libtheora`, `libvpx` and `lame`  

### EXTRA LIBRARIES USED WITH ENABLEGPL=1 ###

`x264`

### LICENSING ###

The resulting FFmpeg is **non GPL** as long ENABLEGPL=1 is not specified

### LICENSE (BUILD SCRIPT) ###
GPLv2
