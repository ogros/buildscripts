#!/usr/bin/env bash

PACKAGE="ffmpeg"

. ${0%/*}/../common/common.inc.sh

download "yasm" "http://www.tortall.net/projects/yasm/releases/yasm-1.2.0.tar.gz"
download "zlib" "http://zlib.net/zlib-1.2.8.tar.gz"
download "bzip2" "http://www.bzip.org/1.0.6/bzip2-1.0.6.tar.gz"
download "libogg" "http://downloads.xiph.org/releases/ogg/libogg-1.3.1.tar.gz"
download "libvorbis" "http://downloads.xiph.org/releases/vorbis/libvorbis-1.3.4.tar.gz"
download "libtheora" "http://downloads.xiph.org/releases/theora/libtheora-1.1.1.tar.bz2"
download "libvpx" "http://webm.googlecode.com/files/libvpx-v1.3.0.tar.bz2"
download "lame" "http://downloads.sourceforge.net/project/lame/lame/3.99/lame-3.99.5.tar.gz?use_mirror=auto"
download "ffmpeg" "http://www.ffmpeg.org/releases/ffmpeg-2.1.3.tar.bz2"
#download "libopus" "http://downloads.xiph.org/releases/opus/opus-1.1.tar.gz"
if [ -n "$ENABLE_GPL" ]; then
download "x264" "ftp://ftp.videolan.org/pub/x264/snapshots/last_stable_x264.tar.bz2"
fi

extract_archives

# yasm
echo_action "building yasm"
pushd yasm*
CC=$CC_NATIVE LD=ld AR=ar RANLIB=ranlib \
./configure --prefix=$TARGET_DIR
$MAKE -j $JOBS
$INSTALL -p -m 755 -D yasm $TARGET_DIR/bin/yasm
popd

# bzip2
echo_action "building libbzip2"
pushd bzip2*
$MAKE CC=$CC $CROSSTOOLS CFLAGS="$CFLAGS -fPIC" LDFLAGS="$LDFLAGS -fPIC" libbz2.a
$INSTALL -p -D libbz2.a $TARGET_DIR/lib/libbz2.a
$INSTALL -p -D bzlib.h $TARGET_DIR/include/bzlib.h
popd


if [ -n "$ENABLE_GPL" ]; then
# x264
echo_action "building libx264"
pushd x264*
test $ISOSX -eq 1 && test $ISCLANG -eq 1 && patch -p0 < $PATCH_DIR/x264-osx-clang-cflags.patch
sed -i'' -e 's/-m32//g' configure # TODO: change to patch
sed -i'' -e 's/-m64//g' configure # TODO: change to patch
if [ $IS64BIT -eq 1 ]; then
    X264CPU="x86_64"
else
    X264CPU="i686"
fi
sed -i'' -e "s/case \$host_cpu/case $X264CPU/g" configure
sed -i'' -e 's/#!\/bin\/bash/#!\/usr\/bin\/env bash/' configure # TODO: change to patch
test -n "$ENABLE_LTO" && patch -p0 < $PATCH_DIR/x264-configure-lto-endian-test.patch
if [ $ISFBSD -eq 1 -a $IS64BIT -eq 0 -a $ISCLANG -eq 1 ]; then
   # http://www.marshut.com/inyihs/failure-to-build-x264-with-asm-on-i386.html
   # This also happens with the OS X target *on* FreeBSD, bug?
   find . -type f \( -name "*.c" -o -name "*.h" \) -print0 | \
     xargs -0 sed -i'' -e "s/if HAVE_X86_INLINE_ASM/if 0/g"
fi
if [ $ISMINGW -eq 1 ]; then
    W32THREAD="--enable-win32thread"
else
    W32THREAD=""
fi
./configure --prefix=$TARGET_DIR $CONFIGURE_FLAGS $W32THREAD \
    --enable-static --enable-pic \
    --disable-cli --disable-avs
$MAKE -j $JOBS install
popd
fi

# ogg
echo_action "building libogg"
pushd libogg*
patch -p0 < $PATCH_DIR/ogg-configure.patch
test $ISOSX -eq 1 && patch -p0 < $PATCH_DIR/ogg-osx-cflags.patch
./configure --prefix=$TARGET_DIR $CONFIGURE_FLAGS --enable-static --disable-shared --with-pic
$MAKE -j $JOBS install
popd

# vorbis
echo_action "building libvorbis"
pushd libvorbis*
patch -p0 < $PATCH_DIR/vorbis-configure.patch
test $ISOSX -eq 1 && patch -p0 < $PATCH_DIR/vorbis-osx.patch
./configure --prefix=$TARGET_DIR $CONFIGURE_FLAGS --enable-static --disable-shared --with-pic
$MAKE -j $JOBS install
popd

# theora
echo_action "building libtheora"
test $IS64BIT -ne 1 && THEORACONFFLAGS="--disable-asm"
pushd libtheora*
test $ISCLANG -ne 0 && patch -p0 < $PATCH_DIR/theora-clang.patch
test $ISFBSD -eq 1 && test $IS64BIT -eq 1 && sed -i'' -e 's/x86_64/amd64/g' config.sub
./configure --prefix=$TARGET_DIR $CONFIGURE_FLAGS $THEORACONFFLAGS \
    --enable-static --disable-shared --with-pic \
    --disable-examples
$MAKE -j $JOBS install
popd

# # opus
# echo_action "building libopus"
# pushd opus*
# ./configure --prefix=$TARGET_DIR $CONFIGURE_FLAGS \
#     --enable-static --disable-shared --with-pic \
#     --enable-rtcd \
#     --enable-asm
# $MAKE -j $JOBS install
# popd

# vpx
echo_action "building libvpx"
pushd libvpx*
VPXTARGETFLAG=""
VPXEXTRAFLAGS=""

[ $ISLINUX -eq 1 -o $ISFBSD -eq 1 ] && VPXEXTRAFLAGS="--extra-cflags=-fvisibility=hidden"

ARCH="x86"

test $IS64BIT -ne 0 && ARCH="x86_64"

if [ $ISLINUX -ne 0 ]; then
    VPXTARGET="linux"
fi
if [ $ISOSX -ne 0 ]; then
    VPXTARGET="darwin9"
fi
if [ $ISFBSD -ne 0 ]; then
    VPXTARGET="linux" # there is no freebsd target...
fi
if [ $ISMINGW -ne 0 ]; then
    VPXTARGET="win"
    if [ $IS64BIT -ne 0 ]; then
        VPXTARGET+="64"
    else
        VPXTARGET+="32"
    fi
fi

test $ISCROSS -ne 0 && test $ISOSX -ne 0 && patch -p0 < $PATCH_DIR/vpx-osx-cross.patch
test $ISMINGW -ne 0 && patch -p0 < $PATCH_DIR/vpx-mingw-strtok_r.patch

VPXTARGETFLAG="--target=${ARCH}-${VPXTARGET}-gcc"

# can't use lto because of yasm
LD=$CC LDFLAGS="${LDFLAGS/-flto/}" CFLAGS="${CFLAGS/-flto/}" \
CXXFLAGS="${CXXFLAGS/-flto/}" ./configure \
 --prefix=$TARGET_DIR $VPXTARGETFLAG $VPXEXTRAFLAGS \
 --disable-shared --enable-pic \
 --disable-docs --disable-examples --disable-unit-tests \
 --enable-runtime-cpu-detect --disable-postproc \
 --enable-vp8 --enable-vp9 --disable-vp8-decoder
$MAKE -j $JOBS install
popd

# lame
echo_action "building libmp3lame"
pushd lame*
./configure --prefix=$TARGET_DIR $CONFIGURE_FLAGS \
    --enable-static --disable-shared --with-pic \
    --disable-frontend
$MAKE -j $JOBS install
popd

# ffmpeg
echo_action "building ffmpeg"
pushd ffmpeg*

FFMPEGCFLAGS="$CFLAGS"
FFMPEGLDFLAGS="$LDFLAGS"
FFMPEGCROSSFLAGS=""
FFMPEGBUILDTYPE="--disable-shared --enable-static"

if [ $ISCROSS ]; then
    ARCH="x86"

    test $IS64BIT -ne 0 && ARCH="x86_64"

    if [ $ISLINUX -ne 0 ]; then
        TARGETOS="linux"
    fi
    if [ $ISOSX -ne 0 ]; then
        TARGETOS="darwin"
    fi
    if [ $ISFBSD -ne 0 ]; then
        TARGETOS="freebsd"
    fi
    if [ $ISMINGW -ne 0 ]; then
        TARGETOS="mingw32"
    fi

    FFMPEGCROSSFLAGS="--arch=$ARCH --target-os=$TARGETOS --cross-prefix=$CROSS --pkg-config=pkg-config"

    test $ISWCLANG -eq 0 && FFMPEGCROSSFLAGS+=" --cc=$CC"  # wclang is passed with make
fi

test $ISOSX -eq 1 && test $IS64BIT -ne 1 && LDFLAGS+=" -read_only_relocs suppress "
test $ISMINGW -eq 1 && FFMPEGBUILDTYPE="--enable-shared --disable-static"
test -n "$ENABLE_LTO" && FFMPEGBUILDTYPE="--enable-shared --enable-static"

if [ -z "$ENABLE_GPL" ]; then
    GPLFLAGS="--disable-gpl --disable-nonfree"
else
    GPLFLAGS="--enable-gpl --enable-libx264"
    if [ $ISLINUX -eq 1 -o $ISOSX -eq 1 ]; then
        FFMPEGLDFLAGS+=" -ldl" # x264
    fi
fi

if [ $ISWCLANG -ne 0 ]; then
    patch -p0 < $PATCH_DIR/ffmpeg-wclang-tree-vectorize.patch
fi

CFLAGS="$FFMPEGCFLAGS" \
LDFLAGS="$FFMPEGLDFLAGS" \
./configure --prefix=$TARGET_DIR \
    $FFMPEGCROSSFLAGS \
    --extra-version=static --disable-debug \
    $FFMPEGBUILDTYPE \
    --enable-pic \
    --disable-doc --disable-htmlpages --disable-manpages --disable-podpages --disable-txtpages \
    --disable-ffmpeg --disable-ffplay --disable-ffprobe --disable-iconv \
    --enable-avresample \
    --enable-runtime-cpudetect \
    --enable-mmx --disable-asm \
    $GPLFLAGS \
    --enable-zlib --enable-bzlib \
    --enable-libmp3lame --enable-libtheora --enable-libvorbis \
    --enable-libvpx

$MAKE CC=$CC -j $JOBS install
popd

finish_libs
