set -e

function echo_bold()
{
    if [ $IS_TERMINAL -eq 1 ]; then
        echo -e "\e[1m${1}\e[0m"
    else
        echo -e "$1"
    fi
}

function echo_yellow()
{
    if [ $IS_TERMINAL -eq 1 ]; then
        echo -e "\e[1m\e[33m${1}\e[0m"
    else
        echo -e "$1"
    fi
}

function echo_red()
{
    if [ $IS_TERMINAL -eq 1 ]; then
        echo -e "\e[1m\e[31m${1}\e[0m"
    else
        echo -e "$1"
    fi
}

function echo_action()
{
    echo_bold "\n*** $1 ***\n"
}

function echo_warning()
{
    echo_yellow "warning: $1" 1>&2
}

function echo_error()
{
    echo_red "error: $1" 1>&2
}

function implode()
{
    local glue
    local result
    declare -i i=0
    declare -a input=("${!2}")

    for ((i=0; i<${#input[@]}; i++)); do
        result+="${glue}${input[$i]}"
        test -z "$glue" && glue="$1"
    done

    echo -n "$result"
}

function exec_silent()
{
    set +e

    OUTPUT=$($* 2>&1)

    if [ $? -ne 0 ]; then
        echo_error "$OUTPUT"
        exit 1
    fi

    set -e
}

function have_prog()
{
    test -z "$1" && return 1

    local have=0
    local prog=`echo -n "$1" | tr '[:lower:]' '[:upper:]' | tr -cd '[:alnum:]'`
    declare -i required="$2"

    have=$(which "$1" &>/dev/null && echo 1 || echo 0)

    eval "HAVE_${prog}"=$have

    if [ "$have" -eq 0 -a $required -ne 0 ]; then
        echo_error "'$1' is required to build '$PACKAGE'"
        exit 1
    fi
}

function download()
{
    pushd $SOURCES_DIR &>/dev/null

    declare -i have=0
    local wget_opts="--trust-server-names --continue"

    if [ $ISNATIVEBSD -eq 1 ]; then
        wget_opts+=" --ca-certificate /usr/local/share/certs/ca-root-nss.crt"
    fi

    test -n "$3" && wget_opts+=" -O $3"

    if [ -f .sources ]; then
        for s in $(cat .sources); do
            if [ "$s" == "$2" ]; then
                have=1
                break
            fi
        done
    fi

    if [ $have -ne 1 ]; then
        rm -rf *$1*
        if [ $IS_TERMINAL -ne 1 ]; then
            wget_opts+=" --quiet"
            echo_bold "downloading $2"
        fi

        wget $wget_opts "$2"
        echo "$2" >> .sources
    fi

    popd &>/dev/null
}

function extract_archive()
{
    local archive="$1"

    if [ ! -f $archive ]; then
        echo_error "couldn't open archive '$archive'"
        exit 1
    fi

    echo_bold "extracting: $archive"

    case $archive in
        *.tgz|*.tbz|*.tar.gz|*.tar.bz2|*.tar.xz)
            tar xf $archive ;;
        *)
            echo_error "not supported archive '$archive'"
            exit 1
    esac
}

function extract_archives()
{
    echo_action "extracting sources"

    local f

    for f in $SOURCES_DIR/*
    do
        extract_archive "$f"
    done
}

function finish_libs()
{
    local f

    if [ -d $TARGET_DIR/lib ]; then
        find $TARGET_DIR/lib/ -type f -name "*.a" | while read f; do
            exec_silent $STRIP $STRIPARGS $f
            exec_silent $RANLIB $f
        done
    fi

    if [ -d $TARGET_DIR/bin ]; then
        find $TARGET_DIR/bin/ -type f -name "*.dll" | while read f; do
            exec_silent $STRIP $STRIPARGS "$f"
        done
    fi
}

function gcc_greater_equal()
{
    have_prog "bc" 1

    local version_a
    local version_b

    [ $ISGCC -ne 1 ] && echo 0 && exit 0

    version_a=$(echo $GCC_VERSION | tr '.' ' ' | awk '{printf "%d.%d", $1, $2}')
    version_b=$(echo $1 | tr '.' ' ' | awk '{printf "%d.%d", $1, $2}')

    echo $(echo "$version_a>=$version_b" | bc -l)
}

export LC_ALL="C"

if [ -t 1 ]; then
    IS_TERMINAL=1
else
    IS_TERMINAL=0
fi


ISLINUX=0
ISOSX=0
ISFBSD=0
ISMINGW=0
ISWCLANG=0

case $TARGET in
    linux32|linux64)
        PLATFORM="Linux"
        ISLINUX=1 ;;
    osx32|osx64)
        PLATFORM="Darwin"
        ISOSX=1 ;;
    freebsd32|freebsd64)
        PLATFORM="FreeBSD"
        ISFBSD=1 ;;
    mingw32|mingw64)
        PLATFORM="MINGW"
        ISMINGW=1 ;;
    w32-clang|w64-clang)
        PLATFORM="MINGW"
        ISMINGW=1
        ISWCLANG=1
        ;;
    *)
        echo_error "invalid TARGET '$TARGET'"
        exit 1
        ;;
esac

if [ -n "$USEGCC" -o $ISMINGW -eq 1 ]; then
    # gcc is prefered, so unset the previous value
    unset CC
    unset CXX
fi

if [ -z "$CC_NATIVE" ] || [ -z "$CXX_NATIVE" ]; then
    have_prog "clang"
    have_prog "gcc"

    if [ $HAVE_CLANG -eq 1 -a -z "$USEGCC" ]; then
        if [ -z "$CC_NATIVE" ]; then CC_NATIVE=clang; fi
        if [ -z "$CXX_NATIVE" ]; then CXX_NATIVE=clang++; fi
    else
        if [ $HAVE_GCC -eq 1 ]; then
            if [ -z "$CC_NATIVE" ]; then CC_NATIVE=gcc; fi
            if [ -z "$CXX_NATIVE" ]; then CXX_NATIVE=g++; fi
        else
            echo_error "you have to set 'CC_NATIVE' and 'CXX_NATIVE'"
            exit 1
        fi
    fi

    if [ -z "$TARGET" ]; then
        echo "TARGET must be set!"
        echo "supported targets:"
        echo " linux32 linux64"
        echo " osx32 osx64"
        echo " freebsd32 freebsd64"
        echo " mingw32 mingw64"
        echo " w32-clang w64-clang"
        exit 1
    fi
fi

if [ -n "$PACKAGE_TARGETS" ]; then
    if [ "${PACKAGE_TARGETS/$TARGET/}" == "$PACKAGE_TARGETS" ]; then
        echo_warning "PACKAGE $PACKAGE doesn't support TARGET $TARGET"
        exit 0
    fi
fi

pushd ${0%/*}
NATIVE_PLATFORM="`uname -s`"
PATCH_DIR="$PWD/patches"
BASE_DIR="$PWD/../build/$PACKAGE"
mkdir -p $BASE_DIR
pushd $BASE_DIR
BUILD_DIR="$PWD/build_$TARGET"
SOURCES_DIR="$PWD/tarballs"
TARGET_DIR="$PWD/target_$TARGET"
rm -rf $BUILD_DIR
rm -rf $TARGET_DIR
mkdir -p $BUILD_DIR
mkdir -p $TARGET_DIR
mkdir -p $SOURCES_DIR
export PATH="$TARGET_DIR/bin:$PATH"
export PKG_CONFIG_PATH="$TARGET_DIR/lib/pkgconfig"

ISCROSS=0
STRIP=strip
RANLIB=ranlib
STRIPARGS=""
CROSSTOOLS=""
CONFIGURE_FLAGS="--libdir=$TARGET_DIR/lib"
FLAVOUR=()
JOBS=${JOBS:=$(../../common/get_cpu_count.sh)}

CFLAGS+=" -O2"
CXXFLAGS+=" -O2"

if [[ "$TARGET" == *64* ]]; then
    CFLAGS+=" -m64"
    CXXFLAGS+=" -m64"
    LDFLAGS+=" -m64"
    IS64BIT=1
else
    CFLAGS+=" -m32"
    CXXFLAGS+=" -m32"
    LDFLAGS+=" -m32"
    IS64BIT=0
fi

if [ $ISWCLANG -ne 0 ]; then
    if [ "$TARGET" != "${TARGET/64/}" ]; then
        CC="w64-clang"
        CXX="w64-clang++"
    else
        CC="w32-clang"
        CXX="w32-clang++"
    fi

    HOST=`$CC -wc-target`

    if [ $? -ne 0 ]; then
        echo_error "couldn't determine wclang host"
        exit 1
    fi
else
    if [ $ISMINGW -ne 0 ] && [ -z "$HOST" ]; then
        if [ $IS64BIT -eq 1 ]; then
            HOST="x86_64-w64-mingw32"
        else
            HOST="i686-w64-mingw32"
        fi
    fi
fi

if [ $ISOSX -eq 1 -a $NATIVE_PLATFORM != "Darwin" -a -z "$HOST" ];
then
    have_prog "osxcross-conf" 1

    eval `osxcross-conf`

    if [ $IS64BIT -eq 1 ]; then
        if [ -n "$USEGCC" ]; then
            CC="x86_64-apple-${OSXCROSS_TARGET}-gcc"
            CXX="x86_64-apple-${OSXCROSS_TARGET}-g++"
        else
            CC="x86_64-apple-${OSXCROSS_TARGET}-clang"
            CXX="x86_64-apple-${OSXCROSS_TARGET}-clang++"
        fi
        HOST="x86_64-apple-${OSXCROSS_TARGET}"
    else
        if [ -n "$USEGCC" ]; then
            CC="i386-apple-${OSXCROSS_TARGET}-gcc"
            CXX="i386-apple-${OSXCROSS_TARGET}-g++"
        else
            CC="i386-apple-${OSXCROSS_TARGET}-clang"
            CXX="i386-apple-${OSXCROSS_TARGET}-clang++"
        fi
        HOST="i386-apple-${OSXCROSS_TARGET}"
    fi
    ISOSXCROSS=1
else
    ISOSXCROSS=0
fi

if [ $NATIVE_PLATFORM == "Darwin" ] || [[ $NATIVE_PLATFORM == *BSD ]];
then
    have_prog "ginstall" 1
    have_prog "gmake" 1
    ISNATIVEBSD=1
    INSTALL=ginstall
    MAKE=gmake
else
    ISNATIVEBSD=0
    INSTALL=install
    MAKE=make
fi

if [ -n "$HOST" ]; then
    ISCROSS=1

    export LD="${HOST}-ld"
    export AR="${HOST}-ar"
    export RANLIB="${HOST}-ranlib"
    export STRIP="${HOST}-strip"

    which $LD &>/dev/null || {
        echo_error "invalid HOST '$HOST' given"
        exit 1
    }

    if [ $ISWCLANG -eq 0 -a $ISOSXCROSS -eq 0 ]; then

        if [ $ISMINGW -eq 0 -a -z "$USEGCC" ]; then
            CC="${HOST}-clang"
            CXX="${HOST}-clang++"
        fi

        set +e

        which "$CC" &>/dev/null
        if [ $? -ne 0 -o -z "$CC" -o -n "$USEGCC" ]; then
            CC="${HOST}-gcc"
            CXX="${HOST}-g++"
        fi

        set -e
    fi

    if [ -z "$CC" ]; then
        echo_error "CC not set"
        exit 1
    fi

    if [ -z "$CXX" ]; then
        echo_error "CXX not set"
        exit 1
    fi

    have_prog $CC 1
    have_prog $CXX 1

    test $ISMINGW -eq 1 && export LDFLAGS+=" -static-libgcc -static-libstdc++ "

    echo ""
    echo "LD = $LD"
    echo "AR = $AR"
    echo "RANLIB = $RANLIB"

    CROSSTOOLS="LD=$LD AR=$AR RANLIB=$RANLIB STRIP=$STRIP"

    CONFIGURE_FLAGS+=" --host=${HOST} "
    export CROSS="${HOST}-" # vpx
else
    if [ $PLATFORM != $NATIVE_PLATFORM ]; then
        echo_error "HOST must be set for target '$PLATFORM'"
        exit 1
    fi
fi

if [ $ISOSX -ne 0 ]; then
    STRIPARGS="-S"
else
    STRIPARGS="--strip-unneeded"
fi

CFLAGS+=" -I$TARGET_DIR/include "
CXXFLAGS+=" -I$TARGET_DIR/include "
LDFLAGS+=" -L$TARGET_DIR/lib "

test -z "$CC" && CC=$CC_NATIVE
test -z "$CXX" && CXX=$CXX_NATIVE

if [[ "$CC" == *clang* ]]; then
    ISCLANG=1
else
    ISCLANG=0
fi

if [[ "$CC" == *gcc* ]]; then
    GCC_VERSION=$($CC -dumpversion)
    ISGCC=1
else
    ISGCC=0
fi

if [ -n "$ENABLE_LTO" ]; then
    LTOFLAG="-flto"

    if [[ $CC == *icc* ]]; then
        LTOFLAG="-ipo"
    fi

    CFLAGS+=" $LTOFLAG"
    LDFLAGS+=" $LTOFLAG"
    CXXFLAGS+=" $LTOFLAG"

    if [ $(gcc_greater_equal 4.8) -eq 1 ]; then
        export CFLAGS+=" -ffat-lto-objects"
        export CXXFLAGS+=" -ffat-lto-objects"
    fi

    FLAVOUR+=("LTO")
fi

if [ -n "$ENABLE_GPL" ]; then
    FLAVOUR+=("GPL")
fi

if [ -z "$ENABLE_CHECKS" ]; then
    CFLAGS+=" -fno-stack-protector -U_FORTIFY_SOURCE"
    CXXFLAGS+=" -fno-stack-protector -U_FORTIFY_SOURCE"
else
    CFLAGS+=" -fstack-protector -D_FORTIFY_SOURCE=2"
    CXXFLAGS+=" -fstack-protector -D_FORTIFY_SOURCE=2"

    if [ -n "$ENABLE_LTO" ]; then
        echo ""
        echo_warning "ENABLE_CHECKS=1 may not play well with ENABLE_LTO=1"
        sleep 2
    fi

    FLAVOUR+=("STACK PROTECTOR")
fi

export CC CXX CFLAGS CXXFLAGS LDFLAGS

if [ "${#FLAVOUR[@]}" -gt 0 ]; then
    FLAVOUR="$(implode ', ' FLAVOUR[@])"
else
    FLAVOUR="default"
fi

echo_action "setting build environment"
echo "$(echo_bold "CC") = $CC"
echo "$(echo_bold "CXX") = $CXX"
echo "$(echo_bold "STRIP") = $STRIP $STRIPARGS"
echo "$(echo_bold "NATIVE CC") = $CC_NATIVE"
echo "$(echo_bold "NATIVE CXX") = $CXX_NATIVE"
echo "$(echo_bold "CFLAGS") = $CFLAGS"
echo "$(echo_bold "CXXFLAGS") = $CXXFLAGS"
echo "$(echo_bold "LDFLAGS") = $LDFLAGS"
echo "$(echo_bold "TARGET") = $TARGET"
echo "$(echo_bold "FLAVOUR") = $FLAVOUR"
echo "$(echo_bold "JOBS") = $JOBS"

pushd $BUILD_DIR &>/dev/null
