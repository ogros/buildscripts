#!/usr/bin/env bash

PACKAGE="mpdclient"

. ${0%/*}/../common/common.inc.sh

download "mpdclient" "http://www.musicpd.org/download/libmpdclient/2/libmpdclient-2.9.tar.xz"

if [ $ISMINGW -eq 1 ]; then
    CONFIGURE_FLAGS+=" --enable-shared --disable-static"
else
    CONFIGURE_FLAGS+=" --disable-shared --enable-static --with-pic"
fi

extract_archives

pushd libmpdclient*
echo_action "building mpdclient"
if [ $ISMINGW -eq 1 ]; then
# should not use #ifdef WIN32 for WIN64
find . -type f \( -name "*.c" -o -name "*.h" \) -print0 | \
  xargs -0 sed -i'' -e "s/#ifdef WIN32/#ifdef _WIN32/g"
fi
./configure --prefix=$TARGET_DIR $CONFIGURE_FLAGS \
    --disable-documentation
$MAKE -j $JOBS install
popd

finish_libs
