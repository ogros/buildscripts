#!/usr/bin/env bash

set -e

pushd "${0%/*}" &>/dev/null

./curl/build.sh
./geoip/build.sh
./mpdclient/build.sh
./pcre/build.sh
./portaudio/build.sh
./sensors/build.sh
./ffmpeg/build.sh
